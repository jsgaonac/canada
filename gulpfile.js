var gulp = require('gulp');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var connect = require('connect');
var serveStatic = require('serve-static');
var gutil = require('gulp-util');
var rename = require('gulp-rename');
var beautifier = require('gulp-jsbeautifier');

gulp.task('watch', ['styles', 'serve'], function() {
    gulp.watch('sass/**/*.scss', ['styles']);
});

gulp.task('styles', function() {
    gulp.src('sass/**/**.scss')
        .pipe(sass().on('error', function(error) {
            return notify().write(error)
        }))
        .pipe(beautifier())
        .pipe(gulp.dest('./css'));
});

gulp.task('serve', function() {
    var port = 3000;
    connect().use(serveStatic(__dirname)).listen(port, function() {
        gutil.log('Serving on: http://localhost:' + port);
    });
});

gulp.task('copy-assets', function() {
    gulp.src('./node_modules/normalize.css/normalize.css')
        .pipe(rename('_normalize.scss'))
        .pipe(gulp.dest('./sass'));
});